document.addEventListener("DOMContentLoaded", function () {
    const message = new XMLHttpRequest();
    message.onreadystatechange = function () {
        if (message.readyState === 4 && message.status === 200) {
            document.getElementById("output").innerText = message.responseText;
        }
    };

    message.open("GET", "../php/hello.php", true);
    message.send();
});
