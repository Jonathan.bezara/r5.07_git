const assert = require('assert');
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest; // Pour Node.js

function testAjaxRequest() {
    const message = new XMLHttpRequest();
    message.onreadystatechange = function () {
        if (message.readyState === 4 && message.status === 200) {
            assert.strictEqual(message.responseText, 'Hello, World!');
            console.log('Test passed!');
        } else if (message.readyState === 4) {
            console.error('Test failed! Unexpected response:', message.responseText);
        }
    };

    message.open('GET', '../php/hello.php', true);
    message.send();
}

// Exécute le test
testAjaxRequest();
