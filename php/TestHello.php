<?php

 // Remplacez par le chemin réel vers votre fichier phpunit.phar


class HelloWorldTest extends PHPUnit\Framework\TestCase
{
    public function testHelloWorldOutput()
    {
        ob_start();
        include 'hello.php'; // Remplacez 'votre_script.php' par le chemin de votre fichier
        $output = ob_get_clean();
        $this->assertEquals("a", $output, "La sortie n'est pas correcte.");
    }
}
