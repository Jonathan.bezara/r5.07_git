# Utilisez une image PHP officielle en fonction de la version dont vous avez besoin
FROM php:7.4-cli

# Installez les dépendances nécessaires
RUN apt-get update \
    && apt-get install -y wget \
    && rm -rf /var/lib/apt/lists/*

# Téléchargez PHPUnit PHAR
RUN wget https://phar.phpunit.de/phpunit-9.phar -O /usr/local/bin/phpunit \
    && chmod +x /usr/local/bin/phpunit

# Créez un répertoire pour vos tests et copiez les fichiers de code source dans le conteneur
WORKDIR /app
COPY php /app/php

# Exécutez votre test PHPUnit
CMD ["phpunit", "php/TestHello.php"]
